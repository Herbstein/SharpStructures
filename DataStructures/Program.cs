﻿namespace DataStructures {
    using System;

    internal class Program {
        private static void Main(string[] args) {
            RpnLoop();
        }

        private static void RpnLoop() {
            while (true) {
                Console.Write("> ");
                var input = Console.ReadLine();
                if (input.Trim().ToLower() == "quit") break;

                var values = new Stack<long>();

                foreach (var token in input.Split(' ')) {
                    long value;
                    if (long.TryParse(token, out value)) {
                        values.Push(value);
                    } else {
                        var rhs = values.Pop();
                        var lhs = values.Pop();

                        switch (token) {
                            case "+":
                                values.Push(lhs + rhs);
                                break;
                            case "-":
                                values.Push(lhs - rhs);
                                break;
                            case "*":
                                values.Push(lhs * rhs);
                                break;
                            case "/":
                                values.Push(lhs / rhs);
                                break;
                            case "%":
                                values.Push(lhs % rhs);
                                break;
                            case "^":
                                values.Push((int)Math.Pow(lhs, rhs));
                                break;
                            default:
                                throw new ArgumentException($"Unrecognized token: {token}");
                        }
                    }
                }

                Console.WriteLine(values.Pop());
            }
        }
    }
}