﻿namespace DataStructures {
    using System;

    internal class Queue<T> {
        private readonly Deque<T> items = new Deque<T>();

        public void Enqueue(T item) {
            items.EnqueueFirst(item);
        }

        public T Dequeue() {
            return items.DequeueLast();
        }

        public T Peek() {
            return items.PeekLast();
        }

        public int Count => items.Count;
    }
}