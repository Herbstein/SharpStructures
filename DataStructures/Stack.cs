﻿namespace DataStructures {
    using System;

    internal class Stack<T> {
        private readonly Deque<T> items = new Deque<T>();

        public int Count => items.Count;

        public void Push(T item) {
            items.EnqueueFirst(item);
        }

        public T Pop() {
            return items.DequeueFirst();
        }

        public T Peek() {
            return items.PeekFirst();
        }
    }
}