﻿namespace DataStructures
{
    using System;

    internal class Deque<T>
    {
        private T[] items = new T[0];
        private int head = 0;
        private int tail = -1;

        public void EnqueueFirst(T item) {
            if (items.Length == Count) {
                AllocateNewArray(1);
            }

            if (head > 0) {
                head--;
            } else {
                head = items.Length - 1;
            }

            items[head] = item;

            Count++;
        }

        public void EnqueueLast(T item) {
            if (items.Length == Count) {
                AllocateNewArray(0);
            }

            if (tail == items.Length - 1) {
                tail = 0;
            } else {
                tail++;
            }

            items[tail] = item;
            Count++;
        }

        public T DequeueFirst() {
            if (Count == 0) {
                throw new InvalidOperationException("The Deque is empty");
            }

            var value = items[head];
            if (head == items.Length - 1) {
                head = 0;
            } else {
                head++;
            }

            Count--;

            return value;
        }

        public T DequeueLast() {
            if (Count == 0) {
                throw new InvalidOperationException("The Deque is empty");
            }

            var value = items[tail];

            if (tail == 0) {
                tail = items.Length - 1;
            } else {
                tail--;
            }

            Count--;

            return value;
        }

        public T PeekFirst() {
            if (Count == 0)
            {
                throw new InvalidOperationException("The Deque is empty");
            }

            return items[head];
        }

        public T PeekLast() {
            if (Count == 0)
            {
                throw new InvalidOperationException("The Deque is empty");
            }

            return items[tail];
        }

        private void AllocateNewArray(int startingIndex) {
            var newLength = (Count == 0) ? 4 : Count * 2;
            var newArray = new T[newLength];

            if (Count > 0) {
                var targetIndex = startingIndex;

                if (tail < head) {
                    for (var index = head; index < items.Length; index++) {
                        newArray[targetIndex++] = items[index];
                    }

                    for (var index = 0; index < tail; index++) {
                        newArray[targetIndex++] = items[index];
                    }
                } else {
                    for (var index = head; index < tail; index++) {
                        newArray[targetIndex++] = items[index];
                    }
                }

                head = startingIndex;
                tail = targetIndex - 1;
            } else {
                head = 0;
                tail = -1;
            }

            items = newArray;
        }

        public int Count { get; private set; }
    }
}
