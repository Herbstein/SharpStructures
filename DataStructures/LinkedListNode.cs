﻿namespace DataStructures {
    internal class LinkedListNode<T> {
        public T Value;

        public LinkedListNode(T value) {
            Value = value;
        }

        public LinkedListNode<T> Next { get; internal set; }
        public LinkedListNode<T> Previous { get; internal set; }
    }
}