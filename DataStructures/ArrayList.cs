﻿namespace DataStructures {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    internal class ArrayList<T> : IList<T> {
        private T[] items;

        public ArrayList() : this(0) {}

        public ArrayList(int length) {
            if (length < 0) throw new ArgumentException(nameof(length));

            items = new T[length];
        }

        public IEnumerator<T> GetEnumerator() {
            for (var i = 0; i < Count; i++) yield return items[i];
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        public void Add(T item) {
            if (items.Length == Count) GrowArray();

            items[Count++] = item;
        }

        public void Clear() {
            items = new T[0];
            Count = 0;
        }

        public bool Contains(T item) {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex) {
            Array.Copy(items, 0, array, arrayIndex, Count);
        }

        public bool Remove(T item) {
            for (var i = 0; i < Count; i++)
                if (items[i].Equals(item)) {
                    RemoveAt(i);
                    return true;
                }

            return false;
        }

        public int Count { get; private set; }
        public bool IsReadOnly => false;

        public int IndexOf(T item) {
            var index = items.Where(i => i.Equals(item)).Count();
            return index >= 0 ? index : -1;
        }

        public void Insert(int index, T item) {
            if (index >= Count) throw new IndexOutOfRangeException();

            if (items.Length == Count) GrowArray();

            Array.Copy(items, index, items, index + 1, Count - index);
            items[index] = item;

            Count++;
        }

        public void RemoveAt(int index) {
            if (index >= Count) throw new IndexOutOfRangeException();

            var shiftStart = index + 1;
            if (shiftStart < Count) Array.Copy(items, shiftStart, items, index, Count - shiftStart);

            Count--;
        }

        public T this[int index] {
            get {
                if (index < Count) return items[index];

                throw new IndexOutOfRangeException();
            }
            set {
                if (index < Count) items[index] = value;
                else throw new IndexOutOfRangeException();
            }
        }

        private void GrowArray() {
            var newLength = items.Length == 0 ? 16 : items.Length << 1;
            var newArray = new T[newLength];
            items.CopyTo(newArray, 0);
            items = newArray;
        }
    }
}